# RevealJsStartingProject

A reveal js template to start a new project with good plugins.

## Plugin list
* [diagram-plugin](https://github.com/teone/reveal.js-diagram-plugin) ([Demo](https://teone.github.io/reveal.js-diagram-plugin/demo/#/)): Plugin to add diagrams with transitions triggered by slide navigation, based on D3.js 
* [menu](https://github.com/denehyg/reveal.js-menu) ([Demo](https://denehyg.github.io/reveal.js-menu/#/home)): A slideout menu to quickly jump to any slide by title. Also optionally changes the theme and sets the default transition.
* [toolbar](https://github.com/denehyg/reveal.js-toolbar) ([Demo](https://denehyg.github.io/reveal.js-toolbar/#/home)): A toolbar to quickly access reveal.js functionality such as fullscreen, notes and pause. compatible with reveal.js-menu
* [verticator](https://github.com/Martinomagnifico/reveal.js-verticator) ([Demo](https://martinomagnifico.github.io/reveal.js-verticator/demo.html#/)): A plugin that adds indicators to show the amount of slides in a vertical stack. Similar to the indicators at fullPage.js.

