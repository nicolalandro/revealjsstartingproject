FROM node:8

EXPOSE 8000

WORKDIR /
RUN git clone --depth 1 https://github.com/hakimel/reveal.js.git

WORKDIR /reveal.js

RUN npm install

RUN npm install --save reveal.js-menu
RUN npm install --save reveal.js-d3js
RUN npm install --save reveal.js-toolbar

CMD npm start

